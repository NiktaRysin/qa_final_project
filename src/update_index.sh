# Обновление индексов на новое поколение
ssh root@142.132.184.58 "cp /root/qa/dgs/dg_2/ indices/ -r"
ssh root@78.47.62.227 "cp /root/qa/dgs/dg_2/ indices/ -r"

# rolling update сервиса индекса кластера
ssh root@142.132.184.58 "docker service update --force --update-parallelism 1 --update-delay 30s qa_index_service_0"
ssh root@142.132.184.58 "docker service update --force --update-parallelism 1 --update-delay 30s qa_index_service_1"
ssh root@142.132.184.58 "docker service update --force --update-parallelism 1 --update-delay 30s qa_index_service_2"
ssh root@142.132.184.58 "docker service update --force --update-parallelism 1 --update-delay 30s qa_index_service_3"

# rolling update сервиса gateway
ssh root@142.132.184.58 "docker service update --force --update-parallelism 1 --update-delay 30s qa_gateway"